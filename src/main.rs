use crc32fast::Hasher;
use exif::{DateTime, In, Tag, Value};
use std::fs;
use std::fs::DirBuilder;
use std::path::Path;
use walkdir::{DirEntry, WalkDir};

struct Args {
    outdir: Result<String, pico_args::Error>,
    indir: Result<String, pico_args::Error>,
    move_file: Result<String, pico_args::Error>,
}

fn main() {
    let mut args = pico_args::Arguments::from_env();
    let args = Args {
        outdir: args.value_from_str("--outdir"),
        indir: args.value_from_str("--indir"),
        move_file: args.value_from_str("--move"),
    };

    let mut move_file = false;

    if let Ok(sd) = args.indir {
        if let Ok(wf) = args.outdir {
            if let Ok(_) = args.move_file {
                move_file = true;
            }

            worker(Path::new(&wf), &sd, move_file);
        }
    }
}

fn worker(work_folder: &Path, start_dir: &str, move_file: bool) {
    let walker = WalkDir::new(start_dir).into_iter();
    let mut entries = walker.filter_entry(|e| !is_hidden(e));
    loop {
        let entry = entries.next();
        match entry {
            Some(entry) => {
                let entry = entry.unwrap();
                if entry.file_type().is_file() && is_image(&entry) {
                    let file = std::fs::File::open(entry.path()).unwrap();
                    let mut bufreader = std::io::BufReader::new(&file);
                    let exifreader = exif::Reader::new();

                    match exifreader.read_from_container(&mut bufreader) {
                        Ok(exif) => {
                            if let Some(dt_orig) =
                                exif.get_field(Tag::DateTimeOriginal, In::PRIMARY)
                            {
                                match dt_orig.value {
                                    Value::Ascii(ref v) => {
                                        if !v.is_empty() {
                                            match DateTime::from_ascii(&v[0]) {
                                                Ok(dt) => {
                                                    create_folders(
                                                        &work_folder,
                                                        &entry,
                                                        &dt,
                                                        move_file,
                                                    );
                                                    continue;
                                                }
                                                Err(e) => {
                                                    println!(
                                                        "{} - {:?} - {:?}",
                                                        entry.file_name().to_str().unwrap(),
                                                        v,
                                                        e
                                                    );
                                                }
                                            }
                                        }
                                    }
                                    _ => {}
                                }
                            }

                            if let Some(dt) = exif.get_field(Tag::DateTime, In::PRIMARY) {
                                match dt.value {
                                    Value::Ascii(ref v) => {
                                        if !v.is_empty() {
                                            match DateTime::from_ascii(&v[0]) {
                                                Ok(dt) => {
                                                    create_folders(
                                                        &work_folder,
                                                        &entry,
                                                        &dt,
                                                        move_file,
                                                    );
                                                    continue;
                                                }
                                                Err(e) => {
                                                    println!(
                                                        "{} - {:?} - {:?}",
                                                        entry.file_name().to_str().unwrap(),
                                                        v,
                                                        e
                                                    );
                                                }
                                            }
                                        }
                                    }
                                    _ => {}
                                }
                            }
                        }
                        Err(e) => {
                            println!("{} - {:#?}", entry.file_name().to_str().unwrap(), e);
                        }
                    }
                }
            }
            _ => {
                break;
            }
        }
    }
}

fn is_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with("."))
        .unwrap_or(false)
}

fn is_image(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| {
            s.to_lowercase().ends_with(".jpg")
                || s.to_lowercase().ends_with(".jpeg")
                || s.to_lowercase().ends_with(".png")
                || s.to_lowercase().ends_with(".heic")
                || s.to_lowercase().ends_with(".webp")
        })
        .unwrap_or(false)
}

fn create_folders(work_folder: &Path, entry: &DirEntry, dt: &DateTime, move_file: bool) {
    let year_f = work_folder.join(dt.year.to_string());
    let month_f = year_f.join(format!("{:02}", dt.month));

    if !year_f.exists() {
        DirBuilder::new().create(year_f.clone()).unwrap();
    }

    if !month_f.exists() {
        DirBuilder::new().create(month_f.clone()).unwrap();
    }

    cp_file(entry, &month_f, move_file);
}

fn cp_file(entry: &DirEntry, work_folder: &Path, move_file: bool) {
    let dest = work_folder.join(entry.file_name());
    let src = entry.path();

    let mut src_hasher = Hasher::new();
    src_hasher.update(&fs::read(&src).unwrap());
    let src_checksum = src_hasher.finalize();

    if dest.exists() {
        let mut dest_hasher = Hasher::new();
        dest_hasher.update(&fs::read(&dest).unwrap());
        let dest_checksum = dest_hasher.finalize();

        if src_checksum == dest_checksum {
            println!("File exists - no copy will be done - {:?}", dest);

            if move_file {
                match fs::remove_file(&src) {
                    Err(e) => println!("{}", e),
                    _ => println!("Deleted {:?}", src),
                }
            }
        }
    } else {
        match fs::copy(src, dest.clone()) {
            Ok(_) => {
                println!("{:?}", src);
                println!("TO");
                println!("{:?}", dest);

                let mut dest_hasher = Hasher::new();
                dest_hasher.update(&fs::read(&dest).unwrap());
                let dest_checksum = dest_hasher.finalize();

                if src_checksum == dest_checksum {
                    println!("Copy successful");

                    if move_file {
                        match fs::remove_file(&src) {
                            Err(e) => println!("{}", e),
                            _ => println!("Deleted {:?}", src),
                        }
                    }
                }
                println!("    ");
            }
            Err(e) => {
                println!("{:?}", src);
                println!("{:?}", dest);
                println!("{}", e);
                println!("    ");
            }
        }
    }
}
